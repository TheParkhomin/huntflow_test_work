import argparse
import asyncio
import logging
import os
import re
from time import time

import aiohttp
import pandas
import redis

logging.basicConfig(format="[%(levelname)s] %(message)s", level=logging.INFO)

file_hundler = logging.FileHandler("file.log")
file_format = logging.Formatter("[%(levelname)s] %(asctime)s %(message)s")

file_hundler.setFormatter(file_format)

logger = logging.getLogger(__name__)
logger.addHandler(file_hundler)


class CustomRedis(redis.Redis):
    """Custom redis class for creating non default connection to redis."""

    def __init__(self):
        host = "redis" if bool(os.getenv("DOCKER_HOST")) else "localhost"
        super().__init__(host=host, decode_responses=True)


class Candidate:
    """
    The Candidate class contains information about candidate.

    :param `**kwargs`: The keyword arguments are used for initializing candidate,
    contains candidate's information.
    """

    SUBMITTED = ("Submitted", 42)
    HR_INTERVIEW = ("HR Interview", 44)
    OFFERED = ("Offered", 46)
    DECLINED = ("Declined", 50)

    STATUS_DICT = {
        "Отправлено письмо": SUBMITTED,
        "Интервью с HR": HR_INTERVIEW,
        "Выставлен оффер": OFFERED,
        "Отказ": DECLINED,
    }

    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.number_line = kwargs.get("number_line")
        self.position = kwargs.get("position")
        self.full_name = kwargs.get("full_name")
        self.salary = kwargs.get("salary")
        self.comment = kwargs.get("comment")
        self.status = self._get_status(kwargs.get("status"))
        self.resume = None
        self.file_path_resume, self.file_name_resume = self._get_file_resume()

    def __str__(self):
        return f"{self.full_name} {self.position}"

    def _get_status(self, old_status):
        """Getting status by statuses dictionary."""

        return self.STATUS_DICT.get(old_status)

    def _get_file_resume(self):
        """Find resume file in some directory by regular expression. """

        regural_expression = (
            self.full_name.strip().replace(" ", "\s{1}") + r"\.\w{3}"
        )
        absolute_path = f"{os.getcwd()}/{self.position}/"
        list_files = "  ".join(os.listdir(absolute_path))
        file_name_resume = re.findall(regural_expression, list_files)[0]
        return f"{absolute_path}{file_name_resume}", file_name_resume


class Resume:
    """
    The Candidate class contains information about candidate.

    :param `**kwargs`: The keyword arguments are used for initializing candidate,
    contains candidate's information.
    """

    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.photo_id = kwargs.get("photo", {}).get("id")
        self.text = kwargs.get("text")

        fields = kwargs.get("fields")
        birthdate = fields.get("birthdate")
        self.birth_day = self.birth_month = self.birth_year = None
        if birthdate:
            self.birth_day = birthdate.get("day")
            self.birth_month = birthdate.get("month")
            self.birth_year = birthdate.get("year")

        self.first_name = fields.get("name").get("first")
        self.last_name = fields.get("name").get("last")
        self.middle_name = fields.get("name").get("middle")
        self.phones = fields.get("phones")
        self.position = fields.get("position")
        self.email = fields.get("email")


async def process_candidate(candidate, session, path):
    """
    Processing of candidate.

    :param candidate: Instance of Candidate class, include information about candidate.
    :type candidate: Candidate

    :param session: session of aiohttp.
    :type session: aiohttp.ClientSession.

    :param path: path to database file.
    :type path: str

    :return: processed candidate.
    :rtype: Candidate
    """

    candidate = await upload_resume(candidate, session)
    candidate = await add_candidate_to_base(candidate, session)
    candidate = await job_posting(candidate, session)

    logger.info(f"END OF PROCESS CANDIDATE {candidate}")
    redis_connection = CustomRedis()

    redis_connection.hdel(path, candidate.number_line)
    redis_connection.close()
    return candidate


def get_candidates_from_xlsx(path):
    """
    Parse excel file.

    :param path: path to excel file.
    :type path: str

    :return: list of Candidate instances:
    :rtype: list
    """

    base_excel = pandas.read_excel(io=path)

    candidates_dict = dict()
    candidates = []
    for i in range(base_excel.last_valid_index() + 1):
        for a in base_excel.to_dict():
            candidates_dict.setdefault(i, {})[a] = base_excel.to_dict()[a][i]

    for key, dict_candidate in candidates_dict.items():
        candidates.append(
            Candidate(
                number_line=key,
                position=dict_candidate.get("Должность"),
                full_name=dict_candidate.get("ФИО"),
                salary=dict_candidate.get("Ожидания по ЗП"),
                comment=dict_candidate.get("Комментарий"),
                status=dict_candidate.get("Статус"),
            )
        )

    return candidates


async def upload_resume(candidate, session):
    """
    Uploading resume file on huntflow server.

    :param candidate: candidate instance.
    :type candidate: Candidate

    :param session: session of aiohttp.
    :type session: aiohttp.ClientSession.

    :return: candidate with resume
    :rtype: Candidate
    """

    url_upload_resume = "https://dev-100-api.huntflow.ru/account/6/upload"
    data = aiohttp.FormData()
    data.add_field(
        "file",
        open(candidate.file_path_resume, "rb"),
        filename=candidate.file_name_resume,
    )
    headers = {"X-File-Parse": "true"}
    async with session.post(
        url_upload_resume, data=data, headers=headers
    ) as response:
        candidate.resume = Resume(**await response.json())
        logger.info(f"UPLOAD RESUME {candidate}")
        return candidate


async def add_candidate_to_base(candidate, session):
    """
    Adding candidate to server.

    :param candidate: candidate instance.
    :type candidate: Candidate

    :param session: session of aiohttp.
    :type session: aiohttp.ClientSession.

    :return: candidate with id
    :rtype: Candidate
    """

    url = "https://dev-100-api.huntflow.ru/account/6/applicants"
    data_candidate = {
        "last_name": candidate.resume.last_name,
        "first_name": candidate.resume.first_name,
        "middle_name": candidate.resume.middle_name,
        "phone": candidate.resume.phones[0],
        "email": candidate.resume.email,
        "position": candidate.resume.position,
        "money": candidate.salary,
        "birthday_day": candidate.resume.birth_day,
        "birthday_month": candidate.resume.birth_month,
        "birthday_year": candidate.resume.birth_year,
        "photo": candidate.resume.photo_id,
        "externals": [
            {
                "data": {"body": candidate.resume.text},
                "auth_type": "NATIVE",
                "files": [{"id": candidate.resume.id}],
            }
        ],
    }

    async with session.post(url, json=data_candidate) as response:
        response_json = await response.json()
        candidate.id = response_json.get("id")
        logger.info(f"ADD {candidate} TO BASE")
        return candidate


async def job_posting(candidate, session):
    """
    Binding candidate to job on server.

    :param candidate: candidate instance.
    :type candidate: Candidate

    :param session: session of aiohttp.
    :type session: aiohttp.ClientSession.

    :return: candidate with job_binding_id
    :rtype: Candidate
    """

    vacancy_url = "https://dev-100-api.huntflow.ru/account/6/vacancies"
    vacancy = None
    async with session.get(vacancy_url) as response:
        response_json = await response.json()
        for item in response_json.get("items"):
            if item.get("position") == candidate.position:
                vacancy = item
                break
    job_posting_url = f"https://dev-100-api.huntflow.ru/account/6/applicants/{candidate.id}/vacancy"
    json_data = {
        "vacancy": vacancy.get("id"),
        "status": candidate.status[1],
        "comment": candidate.comment,
        "rejection_reason": candidate.comment
        if candidate.status[0] == Candidate.DECLINED
        else None,
    }
    async with session.post(job_posting_url, json=json_data) as response:
        response_json = await response.json()
        candidate.job_binding_id = response_json.get("id")
        logger.info(f"JOB POSTING {candidate}")
        return candidate


async def main(candidates, path, access_token):
    """
    Main function. Creating tasks for processing candidates.

    :param candidates: list of Candidate instances.
    :type candidates: list

    :param path: path to database file.
    :type path: str

    :param access_token: token for access on server.
    :type access_token: str
    """

    tasks = []
    headers = {"Authorization": f"Bearer {access_token}"}
    async with aiohttp.ClientSession(headers=headers) as session:
        for candidate in candidates:
            task = asyncio.create_task(
                process_candidate(candidate, session, path)
            )
            tasks.append(task)
        await asyncio.gather(*tasks)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--path", help="This is path to xlsx file.", type=str, required=True
    )

    parser.add_argument(
        "--access_token",
        help="Access token for huntflow API.",
        type=str,
        required=True,
    )

    args = parser.parse_args()

    logger.info(f"START FROM LOCAL PATH {args.path}")
    redis_connection = CustomRedis()
    candidates = get_candidates_from_xlsx(args.path)
    if redis_connection.hgetall(args.path):
        candidates_from_redis = redis_connection.hgetall(args.path)
        candidates = [
            candidate
            for candidate in candidates
            if str(candidate.number_line) in candidates_from_redis.keys()
        ]
    else:
        for candidate in candidates:
            redis_connection.hset(args.path, candidate.number_line, "1")

    redis_connection.close()
    t0 = time()
    asyncio.run(main(candidates, args.path, access_token=args.access_token))

    logger.info(f"{time() - t0} END")
