# Huntflow Testing work

Solution using `asyncio` and `aiohttp`.

## Installation

To build this project, please build images and run them via compose:
```
$ docker-compose build
$ docker-compose up
```

## Run

```
$ docker-compose run huntflow python main.py --path=test_base.xlsx --access_token=<access_token>
```


