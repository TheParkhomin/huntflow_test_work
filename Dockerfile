FROM python:3.8
ENV PYTHONUNBUFFERED 1

RUN apt-get update; apt-get --assume-yes install binutils libproj-dev
RUN mkdir /code/
WORKDIR /code/
ADD . /code/
RUN pip3 install --no-cache-dir -r requirements.txt
